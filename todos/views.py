from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    if form.is_valid():
        list = form.save()
        return redirect("todo_list_detail", id=list.id)
    
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todos = get_object_or_404(TodoList, id=id)
    context = {
        "todos_item": todos,
    }

    return render(request, "todos/details.html", context)
